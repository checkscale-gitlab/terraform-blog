This repository contains simple use cases using the EfficientIP SOLIDserver Hashicorp Terraform provider that could be used when linking the DDI solution to an orchestration process.

* Use case 1: reserve an IP address
* Use case 2: reserve an IP address with a device
* Use case 3: reserve an IP address and set DNS entries for A, PTR and CNAME
* Use case 4: inform IPAM about new IP address of a resource provided by cloud provider

Take a look at the blog entry on [EfficientIP provider for Terraform](https://www.efficientip.com/cloud-automation-terraform-ddi/)

Credentials for the EfficientIP SOLIDserver are located in the `terraform.tfvars` file and should be adapted to your environment.

The `populate.tf` file contains the minimum creation rules to build a new space and populate it for experimentation. All other use cases can be run independently.
