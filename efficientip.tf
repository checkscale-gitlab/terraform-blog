# Configure the SOLIDserver Provider
provider "solidserver" {
  username = "${var.sds_admin}"
  password = "${var.sds_password}"
  host     = "${var.sds_host}"
  sslverify = "${var.sds_check_certificate}"
  version = ">= 1.0.9"
}
