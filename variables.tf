# SOLIDSERVER CREDENTIALS
variable "sds_host" {}

variable "sds_admin" {
   default = "admin_user"
}
variable "sds_password" {
   default = "admin_password"
}
variable "sds_check_certificate" {
   default = true
}
